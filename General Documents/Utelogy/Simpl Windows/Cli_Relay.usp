#CATEGORY "0" // Remote Systems
/*
Dealer Name: Crestron
System Name:
System Number: V1
Programmer: 
Comments:
*/
//Compiler Directives
//#HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#PRINT_TO_TRACE

//INCLUDES

#USER_SIMPLSHARP_LIBRARY "CliSimplSharpLib"

/*******************************************************************************************
  DIGITAL, ANALOG and Relay INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT	_SKIP_,
				Initialise,
			 	Utelogy_Connection,
				Relay_Rx;
DIGITAL_OUTPUT  _SKIP_,_SKIP_,_SKIP_,
				Relay_Tx;

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
CliRelayPort myRelay;
CliMain myCli;
INTEGER initialised, waitTime, debugLevel, debugFB;
INTEGER_PARAMETER PulseTime;
INTEGER_PARAMETER Port;

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
		
   	#BEGIN_PARAMETER_PROPERTIES PulseTime
		propDefaultUnit = unitDecimal;
    	propDefaultValue = 10d; 
		propShortDescription= "1/10ths of a second";
   	#END_PARAMETER_PROPERTIES

	#BEGIN_PARAMETER_PROPERTIES Port
	
	   propValidUnits= unitDecimal;
	   propDefaultValue= 1d;
	
	#END_PARAMETER_PROPERTIES

/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
Callback Function cbf_DebugLevel(SIGNED_INTEGER DebugLvl)		
{
 	debugLevel = DebugLvl;
}

Callback Function cbf_DebugFb(SIGNED_INTEGER DebugFb)		
{
 	debugFB = DebugFb;
}

EventHandler RelayTxEvent (CliRelayPort sender, RelayDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Relay Tx: Port = %d State = %d", e.RelayPort, e.State);
    if (e.RelayPort = Port)
      Relay_Tx = e.State;
}

EventHandler RelayPulseEvent (CliRelayPort sender, RelayDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
      PRINT("Relay Pulse Port = %d", e.RelayPort); 
	if (e.RelayPort = Port)	
    {
	  Relay_Tx = 1;
 	  delay (PulseTime*10);
        Relay_Tx = 0;
    }
}
 
THREADSAFE CHANGE Relay_Rx
{
  if (initialised = 1)
    myRelay.RelayDataReceived(Port, Relay_Rx);
  if (debugLevel >= 1 && debugFB = 1)
    PRINT("Relay Rx: Port = %d State = %d", Port, Relay_Rx);
}

PUSH Utelogy_Connection
{
  //Send Updated Values
  if (initialised = 0)
    waitTime = 1000;
  else
    waitTime = 0;
  
  wait(waitTime)
  {
    myRelay.RelayDataReceived(Port, Relay_Rx);
  }
}

PUSH Initialise
{
  myRelay.Init(Port);
  initialised = 1;
}

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main()
{
    initialised = 0;
    waitTime = 0;
	WaitForInitializationComplete(); 
    //RegisterCoreEvents and Delegates
    RegisterEvent(myRelay, onCommsRelayTxEvent, RelayTxEvent);
	RegisterEvent(myRelay, onCommsRelayPulseEvent, RelayPulseEvent); 
    RegisterDelegate(CliMain, delDebugLvl, cbf_DebugLevel);
    RegisterDelegate(CliMain, delDebugFb, cbf_DebugFb);            
}

