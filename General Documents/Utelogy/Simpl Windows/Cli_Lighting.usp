#CATEGORY "0" // Remote Systems
/*
Dealer Name: Crestron
System Name:
System Number: V1
Programmer: 
Comments:
*/
//Compiler Directives
//#HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#PRINT_TO_TRACE

//INCLUDES

#USER_SIMPLSHARP_LIBRARY "CliSimplSharpLib"

/*******************************************************************************************
  DIGITAL, ANALOG and Relay INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT	_SKIP_,
				Initialise,
				Utelogy_Connected;
DIGITAL_OUTPUT  _SKIP_,
				GroupRecallScene,
				_SKIP_,
				LevelRecallScene,
				_SKIP_,
				GroupRaise[16],
				_SKIP_,
				GroupLower[16],
				_SKIP_,
				GroupOff[16],
				_SKIP_,
				LightRaise[64],
				_SKIP_,
				LightLower[64],
				_SKIP_,
				LightOff[64];
ANALOG_OUTPUT   _SKIP_,
				GroupNumber,
				GroupScene,
				_SKIP_,
				LevelNumber,
				LevelScene,
				_SKIP_,
				LightLevel[64],
				_SKIP_,
				GroupLevel[16];
ANALOG_INPUT	_SKIP_,
				LightLevel_Fb[64],
				_SKIP_,
				GroupLevel_Fb[16];				
								
/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
CliLightingLoop myLighting;
CliMain myCli;
STRING_PARAMETER IPID[2];
INTEGER_PARAMETER LOOP;
INTEGER waiting, initialised, waitTime, debugLevel, debugFB;
STRING loopString[7];
/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
		
   	#BEGIN_PARAMETER_PROPERTIES IPID
   	#END_PARAMETER_PROPERTIES

	#BEGIN_PARAMETER_PROPERTIES LOOP
   	#END_PARAMETER_PROPERTIES


/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
Callback Function cbf_DebugLevel(SIGNED_INTEGER DebugLvl)		
{
 	debugLevel = DebugLvl;
}

Callback Function cbf_DebugFb(SIGNED_INTEGER DebugFb)		
{
 	debugFB = DebugFb;
}

EventHandler LevelSceneRecallTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Level Recall Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
      Pulse(50,LevelRecallScene);
}

EventHandler GroupSceneRecallTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Group Recall Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
      Pulse(50,GroupRecallScene);
}

EventHandler GroupSceneTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Group Scene Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
    {
      GroupNumber = e.Level;
	  GroupScene  = e.Value;
    }
}

EventHandler LevelSceneTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Level Scene Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
    {
      LevelNumber = e.Level;
	  LevelScene = e.Value;
    }
}

EventHandler LightingLevelTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Level Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
    {
	    if (find("Group", e.Name, 1))
	      GroupLevel[e.Level] = e.Value;
	    else
		  LightLevel[e.Level] = e.Value;
    }
}  

EventHandler LightingRaiseTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Raise Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
    {
	    if (find("Group", e.Name, 1))
	      Pulse(50, GroupRaise[e.Level]);
	    else
		  Pulse(50,LightRaise[e.Level]);
    }
}  

EventHandler LightingLowerTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Lower Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
    {
	    if (find("Group", e.Name, 1))
	      Pulse(50, GroupLower[e.Level]);
	    else
		  Pulse(50, LightLower[e.Level]);
    }
}

EventHandler LightingOffTxEvent (CliLightingLoop sender, LightingDataEventArgs e)
{
    if (debugLevel >= 1 && debugFB = 1)
	  PRINT("Lighting Level Off Tx: Name = %s Level = %d Value = %d", e.Name, e.Level, e.Value);
    if (find(loopString, e.Name, 1))
    {
	    if (find("Group", e.Name, 1))
	      Pulse(50, GroupOff[e.Level]);
	    else
		  Pulse(50, LightOff[e.Level]);
    }
}

THREADSAFE CHANGE LightLevel_Fb
{
  integer i;
  i = getlastmodifiedarrayindex();
  if (debugLevel >= 1 && debugFB = 1)
    PRINT("Lighting Level Rx: Port = %d State = %d", i, LightLevel_Fb[i]);
  if (initialised = 1)
    myLighting.LightLevelDataReceived(i, LightLevel_Fb[i]);
}

THREADSAFE CHANGE GroupLevel_Fb
{
  integer i;
  i = getlastmodifiedarrayindex();
  if (initialised = 1)
    myLighting.GroupLevelDataReceived(i, GroupLevel_Fb[i]);
  if (debugLevel >= 1 && debugFB = 1)
    PRINT("Lighting Group Level Rx: Port = %d State = %d", i, GroupLevel_Fb[i]);
}

THREADSAFE PUSH Initialise
{
  integer i;
  // Initiliase Module
  myLighting.Init(IPID, LOOP);
  initialised = 1;
}

THREADSAFE PUSH Utelogy_Connected
{
  //Send Updated Values
  if (initialised = 0)
    waitTime = 1000;
  else
    waitTime = 0;
  
  wait(waitTime)
  {
    integer i;
    for (i = 1 to 64)
    {
      myLighting.LightLevelDataReceived(i, LightLevel_Fb[i]);
      if (i <= 16)
      {
        myLighting.GroupLevelDataReceived(i, GroupLevel_Fb[i]);
      }
    }
  }   
}

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main()
{
	waiting = 0;
    initialised = 0;
    
	WaitForInitializationComplete(); 
    loopString = "Loop" + ITOA(LOOP);
    //RegisterCoreEvents and Delegates
    RegisterEvent(myLighting, onLevelSceneRecallTxEvent, LevelSceneRecallTxEvent);
	RegisterEvent(myLighting, onGroupSceneRecallTxEvent, GroupSceneRecallTxEvent); 
	RegisterEvent(myLighting, onLightingLevelSceneTxEvent, LevelSceneTxEvent);
	RegisterEvent(myLighting, onLightingGroupSceneTxEvent, GroupSceneTxEvent); 
	RegisterEvent(myLighting, onLightingRaiseTxEvent, LightingRaiseTxEvent); 
	RegisterEvent(myLighting, onLightingLowerTxEvent, LightingLowerTxEvent); 
	RegisterEvent(myLighting, onLightingLevelTxEvent, LightingLevelTxEvent);    
	RegisterEvent(myLighting, onLightingOffTxEvent, LightingOffTxEvent);       
    RegisterDelegate(CliMain, delDebugLvl, cbf_DebugLevel);
    RegisterDelegate(CliMain, delDebugFb, cbf_DebugFb);   
}

